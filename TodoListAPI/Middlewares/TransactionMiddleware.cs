﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TodoListAPI.DAL.EntityFramework;
using TodoListAPI.DAL.Interfaces;

namespace TodoListAPI.Middlewares
{
    public class TransactionMiddleware
    {
        readonly RequestDelegate _next;

        public TransactionMiddleware(RequestDelegate next)
        {
            this._next = next;
        }

        public async Task InvokeAsync(HttpContext context, IUnitOfWork unitOfWork)
        {
            try
            {
                await unitOfWork.BeginTransaction();

                await _next.Invoke(context);

                await unitOfWork.Commit();
            }
            catch (Exception)
            {
                unitOfWork.Rollback();
                throw;
            }
        }
    }
}
