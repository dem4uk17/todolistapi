﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TodoListAPI.Configuration
{
    public static class AuthenticationConfig
    {
        public static string Issuer { get; } = "Issuer";
        public static string Audience { get; } = "Audience";
        public static string Key { get; } = "super_secret_key_123";
        public static int LifeTimeInDays { get; } = 1;

        public static SymmetricSecurityKey GetSymmetricSecurityKey()
        {
            return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(Key));
        }
    }
}
