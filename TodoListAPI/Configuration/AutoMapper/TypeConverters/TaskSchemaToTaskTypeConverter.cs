﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using TodoListAPI.DAL.Entities;
using TodoListAPI.DAL.Interfaces;
using TodoListAPI.Exceptions;
using TodoListAPI.Models.Schemas;

namespace TodoListAPI.Configuration.AutoMapper.TypeConverters
{
    public class TaskSchemaToTaskTypeConverter : ITypeConverter<AddTaskSchema, Task>
    {
        IRepository<User> _userRepository;
        IRepository<TaskStatus> _taskStatusRepository;

        public TaskSchemaToTaskTypeConverter(IRepository<User> userRepository, IRepository<TaskStatus> taskStatusRepository)
        {
            _userRepository = userRepository;
            _taskStatusRepository = taskStatusRepository;
        }

        public Task Convert(AddTaskSchema source, Task destination, ResolutionContext context)
        {
            destination = new Task()
            {
                Description = source.Description,
                Title = source.Title
            };

            destination.TaskStatus = _taskStatusRepository.All().FirstOrDefault(x => x.Id == source.TaskStatusId);
            if (destination.TaskStatus == null) throw new NotFoundEntityException($"Not found entity 'TaskStatus' with id {source.TaskStatusId}");

            destination.User = _userRepository.All().FirstOrDefault(x => x.Id == source.UserId);
            if (destination.TaskStatus == null) throw new NotFoundEntityException($"Not found entity 'User' with id {source.UserId}");

            return destination;
        }
    }
}
