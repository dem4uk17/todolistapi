﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using TodoListAPI.Configuration.AutoMapper.TypeConverters;
using TodoListAPI.DAL.Entities;
using TodoListAPI.Models.Schemas;

namespace TodoListAPI.Configuration.AutoMapper
{
    public class SchemasProfile: Profile
    {
        public SchemasProfile()
        {
            CreateMap<Task, ViewTaskSchema>()
                .ForMember(x => x.TaskStatusId, opt => opt.MapFrom(x => x.TaskStatus.Id))
                .ForMember(x => x.UserId, opt => opt.MapFrom(x => x.User.Id));

            CreateMap<AddTaskSchema, Task>().ConvertUsing<TaskSchemaToTaskTypeConverter>();


            //CreateMap<TaskSchema, Task>()
            //    .ForMember(x => x.TaskStatus, opt => opt.MapFrom(x => new TaskStatus() { Id = x.TaskStatusId }))
            //    .ForMember(x => x.User, opt => opt.MapFrom(x => new User() { Id = x.UserId }));
        }
    }
}
