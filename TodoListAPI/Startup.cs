﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using TodoListAPI.DAL.EntityFramework;
using TodoListAPI.DAL.Extensions;
using TodoListAPI.Extensinons;

namespace TodoListAPI
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IHostingEnvironment environment)
        {
            Configuration = new ConfigurationBuilder()
                .SetBasePath(environment.ContentRootPath)
                .AddJsonFile("dbconfig.json")
                .Build();
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddEntityFrameworkUnitOfWork();
            services.AddEntityFrameworkRepositories();
            
            services.AddDbContext<TodoListContext>(options => 
            {
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"));
            });

            services.AddJWTAuthentication();

            services.AddAutoMapper();

            services.AddMvc();
        }
        
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            //if (env.IsDevelopment())
            //{
            //    app.UseDeveloperExceptionPage();
            //}
            
            app.UseExceptionHandlerMiddleware();

            app.UseTransactionMiddleware();
            app.UseAuthentication();
            app.UseMvc();
        }
    }
}
