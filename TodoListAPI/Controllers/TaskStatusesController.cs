﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TodoListAPI.DAL.Entities;
using TodoListAPI.DAL.Interfaces;

namespace TodoListAPI.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class TaskStatusesController : Controller
    {
        IRepository<DAL.Entities.TaskStatus> _taskStatusRepository;

        public TaskStatusesController(IRepository<DAL.Entities.TaskStatus> taskStatusRepository)
        {
            _taskStatusRepository = taskStatusRepository;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            return Json(await _taskStatusRepository.All().ToListAsync());
        }        
    }
}
