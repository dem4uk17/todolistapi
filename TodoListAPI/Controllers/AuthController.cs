﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Authentication;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TodoListAPI.DAL.Entities;
using TodoListAPI.DAL.Interfaces;
using TodoListAPI.Services;

namespace TodoListAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : Controller
    {
        IRepository<User> _userRepository;
        AuthenticationService _authenticationService;

        public AuthController(IRepository<User> userRepository, AuthenticationService authenticationService)
        {
            _userRepository = userRepository;
            _authenticationService = authenticationService;
        }

        [HttpGet]
        public async Task<IActionResult> Authenticate([FromQuery] string username, string password)
        {
            var user = await _userRepository.All().Include(x => x.UserRole).FirstOrDefaultAsync(x => x.Username == username);
            if (user == null) throw new InvalidCredentialException("Invalid username");
            if (user.Password != password) throw new InvalidCredentialException("Invalid password");

            var jwtToken = _authenticationService.Authenticate(user);

            return Json(new { token = new JwtSecurityTokenHandler().WriteToken(jwtToken) });
        }
    }
}