﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TodoListAPI.DAL.Entities;
using TodoListAPI.DAL.Interfaces;
using TodoListAPI.Exceptions;
using TodoListAPI.Filters;
using TodoListAPI.Models.Schemas;

namespace TodoListAPI.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class TasksController : Controller
    {
        readonly IMapper _mapper;
        readonly IRepository<Task> _taskRepository;
        readonly IRepository<TaskStatus> _taskStatusRepository;

        public TasksController(IMapper mapper, IRepository<Task> taskRepository, IRepository<TaskStatus> taskStatusRepository)
        {
            _mapper = mapper;
            _taskRepository = taskRepository;
            _taskStatusRepository = taskStatusRepository;
        }

        [UserRoleFilter, HttpGet]
        public async System.Threading.Tasks.Task<IActionResult> Get([FromQuery] int? taskStatusId)
        {
            var tasks = _taskRepository.All().Include(x => x.TaskStatus).Include(x => x.User).AsQueryable();
            
            if (taskStatusId != null)
                tasks = tasks.Where(x => x.TaskStatus.Id == taskStatusId);

            return Json(_mapper.Map<IEnumerable<Task>, IEnumerable<ViewTaskSchema>>(await tasks.ToArrayAsync()));
        }
        
        [HttpPost]
        public async System.Threading.Tasks.Task Post([FromBody] AddTaskSchema taskSchema)
        {
            var task = _mapper.Map<AddTaskSchema, Task>(taskSchema);

            await _taskRepository.AddAsync(task);
        }

        [UserRoleFilter, HttpPut]
        public async System.Threading.Tasks.Task Put([FromQuery] int id, [FromBody] UpdateStatusTaskSchema updateStatusTaskSchema)
        {
            var task = await _taskRepository.All().FirstOrDefaultAsync(x => x.Id == id);
            if (task == null) throw new NotFoundEntityException($"Not found entity 'Task' with id {id}");

            task.TaskStatus = await _taskStatusRepository.All().FirstOrDefaultAsync(x => x.Id == updateStatusTaskSchema.TaskStatusId);
            if (task.TaskStatus == null) throw new NotFoundEntityException($"Not found entity 'TaskStatus' with id {updateStatusTaskSchema.TaskStatusId}");

            _taskRepository.Update(task);
        }

        [UserRoleFilter, HttpDelete]
        public async System.Threading.Tasks.Task Delete([FromQuery] int id)
        {
            var task = await _taskRepository.All().FirstOrDefaultAsync(x => x.Id == id);
            if (task == null) throw new NotFoundEntityException($"Not found entity 'Task' with id {id}");

            _taskRepository.Delete(task);
        }
    }
}
