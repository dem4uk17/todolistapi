﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TodoListAPI.Exceptions
{
    public class NotFoundEntityException: Exception
    {
        public NotFoundEntityException(string message): base(message)
        {
        }
    }
}
