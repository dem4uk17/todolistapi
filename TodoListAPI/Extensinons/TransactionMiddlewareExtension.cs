﻿using Microsoft.AspNetCore.Builder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TodoListAPI.Middlewares;

namespace TodoListAPI.Extensinons
{
    public static class TransactionMiddlewareExtension
    {
        public static IApplicationBuilder UseTransactionMiddleware(this IApplicationBuilder applicationBuilder)
        {
            return applicationBuilder.UseMiddleware<TransactionMiddleware>();
        }
    }
}
