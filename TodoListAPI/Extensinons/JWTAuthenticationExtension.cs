﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TodoListAPI.Configuration;
using TodoListAPI.Services;

namespace TodoListAPI.Extensinons
{
    public static class JWTAuthenticationExtension
    {
        public static IServiceCollection AddJWTAuthentication(this IServiceCollection services)
        {
            services.AddSingleton<AuthenticationService>();

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme).AddJwtBearer(opt =>
            {
                opt.RequireHttpsMetadata = false;
                opt.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuer = true,
                    ValidIssuer = AuthenticationConfig.Issuer,

                    ValidateAudience = true,
                    ValidAudience = AuthenticationConfig.Audience,

                    ValidateLifetime = true,

                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = AuthenticationConfig.GetSymmetricSecurityKey()
                };
            });

            return services;
        }
    }
}