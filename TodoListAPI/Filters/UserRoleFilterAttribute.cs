﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Authentication;
using System.Security.Claims;
using TodoListAPI.Controllers;
using TodoListAPI.DAL.Entities;
using TodoListAPI.DAL.EntityFramework.Repositories.Abstract;
using TodoListAPI.DAL.Interfaces;

namespace TodoListAPI.Filters
{
    public class UserRoleFilterAttribute : TypeFilterAttribute
    {
        public UserRoleFilterAttribute() : base(typeof(UserRoleFilter))
        {
        }

        private class UserRoleFilter : IAsyncActionFilter
        {
            IRepository<Task> _taskRepository;
            //IRepository<User> _userRepository;

            public UserRoleFilter(IRepository<Task> taskRepository/*, IRepository<User> userRepository*/)
            {
                _taskRepository = taskRepository;
                //_userRepository = userRepository;
            }

            async System.Threading.Tasks.Task IAsyncActionFilter.OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
            {
                if (!int.TryParse(context.HttpContext.User.Claims.FirstOrDefault(x => x.Type == ClaimTypes.Name)?.Value, out int userId))
                    throw new AuthenticationException("Cannot parse userId from JWT token");

                var userRoleName = context.HttpContext.User.Claims.FirstOrDefault(x => x.Type == ClaimTypes.Role)?.Value;
                if (String.IsNullOrEmpty(userRoleName))
                    throw new AuthenticationException("Cannot parse user role from JWT token");

                //var user = _userRepository.All().Include(x => x.UserRole).FirstOrDefault(x => x.Id == userId);

                if (userRoleName != "Administrator" && _taskRepository is IFilterableRepository<Task> taskFilterableRepository)
                    taskFilterableRepository.SetFilterExpression(x => x.User.Id == userId);

                await next();
            }
        }
    }
}
