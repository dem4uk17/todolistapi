﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using TodoListAPI.Configuration;
using TodoListAPI.DAL.Entities;

namespace TodoListAPI.Services
{
    public class AuthenticationService
    {
        public JwtSecurityToken Authenticate(User user)
        {
            DateTime now = DateTime.UtcNow;

            var token = new JwtSecurityToken(
                issuer: AuthenticationConfig.Issuer,
                audience: AuthenticationConfig.Audience,
                notBefore: now,
                claims: GetIdentity(user).Claims,
                expires: now.AddDays(AuthenticationConfig.LifeTimeInDays),
                signingCredentials: new SigningCredentials(AuthenticationConfig.GetSymmetricSecurityKey(), SecurityAlgorithms.HmacSha256)
            );

            return token;
        }

        private ClaimsIdentity GetIdentity(User user)
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, user.Id.ToString()),
                new Claim(ClaimsIdentity.DefaultRoleClaimType, user.UserRole.Name)
            };

            return new ClaimsIdentity(claims, "Token", ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);
        }
    }
}
