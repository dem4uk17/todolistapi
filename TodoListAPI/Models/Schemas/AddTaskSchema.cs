﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TodoListAPI.Models.Schemas
{
    public class AddTaskSchema
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public int TaskStatusId { get; set; }
        public int UserId { get; set; }
    }
}
