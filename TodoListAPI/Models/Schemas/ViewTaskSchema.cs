﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TodoListAPI.Models.Schemas
{
    public class ViewTaskSchema: AddTaskSchema
    {
        public int Id { get; set; }
    }
}
