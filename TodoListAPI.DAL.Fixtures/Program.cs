﻿using System;
using System.Collections.Generic;
using TodoListAPI.DAL.Entities;
using TodoListAPI.DAL.EntityFramework;

namespace TodoListAPI.DAL.Fixtures
{
    class Program
    {
        static void Main(string[] args)
        {
            var userRoles = new List<UserRole>()
            {
                new UserRole(){ Name = "User" },
                new UserRole(){ Name = "Administrator" },
            };

            var users = new List<User>()
            {
                new User() { Username = "User", Password = "User", UserRole = userRoles[0] },
                new User() { Username = "Administrator", Password = "Administrator", UserRole = userRoles[1] }
            };

            var taskStatuses = new List<TaskStatus>()
            {
                new TaskStatus() { Name = "Todo" },
                new TaskStatus() { Name = "Done" }
            };

            var tasks = new List<Task>()
            {
                new Task()
                {
                    Title = "First task",
                    Description = "Task description",
                    TaskStatus = taskStatuses[0],
                    User = users[0]
                },
                new Task()
                {
                    Title = "Second task",
                    Description = "Task description",
                    TaskStatus = taskStatuses[0],
                    User = users[1]
                }
            };

            using (var db = new TodoListContextFactory().CreateDbContext(null))
            {
                db.UserRoles.AddRange(userRoles);
                db.Users.AddRange(users);
                db.TaskStatuses.AddRange(taskStatuses);
                db.Tasks.AddRange(tasks);

                db.SaveChanges();
            }

            Console.WriteLine("Done!");
            Console.ReadKey();
        }
    }
}
