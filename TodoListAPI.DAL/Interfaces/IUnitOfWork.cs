﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace TodoListAPI.DAL.Interfaces
{
    public interface IUnitOfWork
    {
        Task BeginTransaction();
        Task Commit();
        void Rollback();
    }
}
