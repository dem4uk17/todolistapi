﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TodoListAPI.DAL.Entities.Abstract;

namespace TodoListAPI.DAL.Interfaces
{
    public interface IRepository<TEntity> where TEntity: BaseEntity
    {
        IQueryable<TEntity> All();
        Task AddAsync(TEntity entity);
        void Update(TEntity entity);
        void Delete(TEntity entity);
    }
}
