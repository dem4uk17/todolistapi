﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using TodoListAPI.DAL.Entities.Abstract;

namespace TodoListAPI.DAL.Interfaces
{
    public interface IFilterableRepository<TEntity> where TEntity: BaseEntity
    {
        void SetFilterExpression(Expression<Func<TEntity, bool>> expression);
    }
}
