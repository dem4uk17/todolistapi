﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TodoListAPI.DAL.Entities;
using TodoListAPI.DAL.EntityFramework.Repositories.Abstract;
using TodoListAPI.DAL.Interfaces;

namespace TodoListAPI.DAL.Repositories.EntityFramework
{
    public class UserRepository : BaseRepository<User>
    {
        public UserRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        protected override IQueryable<User> GetEntities()
        {
            return Database.Users;
        }
    }
}
