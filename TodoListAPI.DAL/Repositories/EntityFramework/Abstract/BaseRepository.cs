﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using TodoListAPI.DAL.Entities.Abstract;
using TodoListAPI.DAL.Interfaces;
using TodoListAPI.DAL.UnitOfWorks.EntityFramework;

namespace TodoListAPI.DAL.EntityFramework.Repositories.Abstract
{
    public abstract class BaseRepository<TEntity>: IRepository<TEntity> where TEntity: BaseEntity
    {
        protected readonly TodoListContext Database;
        protected IQueryable<TEntity> Entities;

        public BaseRepository(IUnitOfWork unitOfWork)
        {
            Database = ((UnitOfWork)unitOfWork).TodoListContext;

            Entities = GetEntities();
        }

        protected abstract IQueryable<TEntity> GetEntities();

        public IQueryable<TEntity> All()
        {
            return Entities;
        }

        public async Task AddAsync(TEntity entity)
        {
            await Database.AddAsync(entity);
        }

        public void Update(TEntity entity)
        {
            Database.Update(entity);
        }

        public void Delete(TEntity entity)
        {
            Database.Remove(entity);
        }
    }
}