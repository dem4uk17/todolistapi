﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using TodoListAPI.DAL.Entities;
using TodoListAPI.DAL.EntityFramework.Repositories.Abstract;
using TodoListAPI.DAL.Interfaces;

namespace TodoListAPI.DAL.Repositories.EntityFramework
{
    public class TaskRepository : BaseRepository<Task>, IFilterableRepository<Task>
    {
        public TaskRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
        
        public void SetFilterExpression(Expression<Func<Task, bool>> expression)
        {
            Entities = Entities.Where(expression);
        }

        protected override IQueryable<Task> GetEntities()
        {
            return Database.Tasks;
        }
    }
}
