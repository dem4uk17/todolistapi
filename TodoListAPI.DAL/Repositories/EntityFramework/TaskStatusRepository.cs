﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TodoListAPI.DAL.Entities;
using TodoListAPI.DAL.EntityFramework.Repositories.Abstract;
using TodoListAPI.DAL.Interfaces;

namespace TodoListAPI.DAL.Repositories.EntityFramework
{
    public class TaskStatusRepository : BaseRepository<TaskStatus>
    {
        public TaskStatusRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        protected override IQueryable<TaskStatus> GetEntities()
        {
            return Database.TaskStatuses;
        }
    }
}
