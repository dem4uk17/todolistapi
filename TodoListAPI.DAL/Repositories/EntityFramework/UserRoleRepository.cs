﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TodoListAPI.DAL.Entities;
using TodoListAPI.DAL.EntityFramework.Repositories.Abstract;
using TodoListAPI.DAL.Interfaces;

namespace TodoListAPI.DAL.Repositories.EntityFramework
{
    class UserRoleRepository : BaseRepository<UserRole>
    {
        public UserRoleRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        protected override IQueryable<UserRole> GetEntities()
        {
            return Database.UserRoles;
        }
    }
}
