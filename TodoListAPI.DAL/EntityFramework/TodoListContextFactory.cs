﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace TodoListAPI.DAL.EntityFramework
{
    public class TodoListContextFactory : IDesignTimeDbContextFactory<TodoListContext>
    {
        public TodoListContext CreateDbContext(string[] args)
        {
            var configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("dbconfig.json")
                .Build();
            
            var optionsBuilder = new DbContextOptionsBuilder<TodoListContext>();
            optionsBuilder.UseSqlServer(configuration.GetConnectionString("DefaultConnection"));

            return new TodoListContext(optionsBuilder.Options);
        }
    }
}
