﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using TodoListAPI.DAL.Entities;

namespace TodoListAPI.DAL.EntityFramework
{
    public class TodoListContext: DbContext
    {
        public DbSet<Task> Tasks { get; set; }
        public DbSet<TaskStatus> TaskStatuses { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<UserRole> UserRoles { get; set; }

        public TodoListContext(DbContextOptions<TodoListContext> options) : base(options)
        {
            Database.Migrate();
        }
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TaskStatus>().HasAlternateKey(u => new { u.Name });
            modelBuilder.Entity<UserRole>().HasAlternateKey(u => new { u.Name });
            modelBuilder.Entity<User>().HasAlternateKey(u => new { u.Username });
            
            base.OnModelCreating(modelBuilder);
        }
    }
}