﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using TodoListAPI.DAL.Entities.Abstract;

namespace TodoListAPI.DAL.Entities
{
    public class TaskStatus : BaseEntity
    {
        [Required, MaxLength(10)]
        public string Name { get; set; }
    }
}
