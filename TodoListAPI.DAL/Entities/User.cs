﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using TodoListAPI.DAL.Entities.Abstract;

namespace TodoListAPI.DAL.Entities
{
    public class User : BaseEntity
    {
        [Required, MinLength(6), MaxLength(40)]
        public string Username { get; set; }
        [Required, MinLength(8), MaxLength(40)]
        public string Password { get; set; }

        [Required]
        public UserRole UserRole { get; set; }
    }
}
