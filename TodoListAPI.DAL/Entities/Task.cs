﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using TodoListAPI.DAL.Entities.Abstract;

namespace TodoListAPI.DAL.Entities
{
    public class Task: BaseEntity
    {
        [Required, MaxLength(100)]
        public string Title { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public TaskStatus TaskStatus { get; set; }
        [Required]
        public User User { get; set; }
    }
}
