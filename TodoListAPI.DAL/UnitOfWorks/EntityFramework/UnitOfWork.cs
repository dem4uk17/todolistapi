﻿using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TodoListAPI.DAL.EntityFramework;
using TodoListAPI.DAL.Interfaces;

namespace TodoListAPI.DAL.UnitOfWorks.EntityFramework
{
    public class UnitOfWork : IUnitOfWork
    {
        public UnitOfWork(TodoListContext todoListContext)
        {
            TodoListContext = todoListContext;
        }

        public TodoListContext TodoListContext { get; }

        IDbContextTransaction _dbContextTransaction;

        public async Task BeginTransaction()
        {
            _dbContextTransaction = await TodoListContext.Database.BeginTransactionAsync();
        }

        public async Task Commit()
        {
            await TodoListContext.SaveChangesAsync();
            _dbContextTransaction.Commit();
            _dbContextTransaction.Dispose();
        }

        public void Rollback()
        {
            _dbContextTransaction.Rollback();
            _dbContextTransaction.Dispose();
        }
    }
}
