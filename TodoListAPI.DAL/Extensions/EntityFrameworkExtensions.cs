﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;
using TodoListAPI.DAL.Entities;
using TodoListAPI.DAL.Interfaces;
using TodoListAPI.DAL.Repositories.EntityFramework;
using TodoListAPI.DAL.UnitOfWorks.EntityFramework;

namespace TodoListAPI.DAL.Extensions
{
    public static class EntityFrameworkExtensions
    {
        public static IServiceCollection AddEntityFrameworkUnitOfWork(this IServiceCollection services)
        {
            services.AddScoped<IUnitOfWork, UnitOfWork>();

            return services;
        }

        public static IServiceCollection AddEntityFrameworkRepositories(this IServiceCollection services)
        {
            services.AddScoped<IRepository<User>, UserRepository>();
            services.AddScoped<IRepository<UserRole>, UserRoleRepository>();
            services.AddScoped<IRepository<Task>, TaskRepository>();
            services.AddScoped<IRepository<TaskStatus>, TaskStatusRepository>();

            return services;
        }
    }
}
